# Kahoot Replica (Kahoot Wannabe)

## Step to run the program

* **client**

  run using command: _**python client/app/app-cli.py**_

* **server**

  run using command: _**python server/app/app-cli.py**_

To see more detailed process in app, uncomment the logging part in both **server/app/app-cli.py** and **client/app/app-cli.py** _main_ function.
