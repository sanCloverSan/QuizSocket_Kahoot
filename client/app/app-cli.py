# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 12:51:22 2020

__author__      = "Christian Patrick"
__copyright__   = "Copyright 2020, Planet Earth"
"""



import os
import sys
import logging
import threading
from datetime import datetime, timedelta
import time, msvcrt

#This part of code should be placed before any module import within this project
#example: findUnusedPort module from utils.FindPort
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
grandparentdir = os.path.dirname(parentdir)
sys.path.append(grandparentdir)

from client.model.TCPClient import TCPClient

JOIN_GAME_WAITING_TIME = 10
GET_ALIAS_WAITING_TIME = 10
ANSWER_WAITING_TIME = 10
SUBMIT_WAITING_TIME = 5

class ClientAppCLI():
    def __init__(self):
        self.__socketObj = TCPClient()
        self.__uname = ""
    
    def run(self, addr:str, port:int):
        print("Starting client app")


        #Prompt for username
        logging.debug("#######################")
        print("")

        print("Input your username:")
        
        starTime = datetime.now()
        self.__uname = self.readInput("Your alias / username", "Bob", ANSWER_WAITING_TIME)
        stopTime = datetime.now()
        print("Username: " + self.__uname)
        peerName = (self.__socketObj.getIPPort())
        print("")

        if ANSWER_WAITING_TIME - ( stopTime -starTime).total_seconds() > 0:
            time.sleep(ANSWER_WAITING_TIME - ( stopTime -starTime).total_seconds() + 1)

        #Connect to server
        successMessage = self.__socketObj.connectToServer(addr, port)

        if successMessage and len(successMessage):
            if "status" in successMessage:
                print("Connection status:" + successMessage["status"], end="\n\n") 

            if "rem_acc_time" in successMessage:
                try:
                    sleepTime = int(successMessage["rem_acc_time"]) + 2
                    time.sleep(sleepTime)
                except Exception as e:
                    logging.debug("Exception: " + str(e))
    

        data = {"alias": self.__uname, "addr" : self.__socketObj.getIPPort()}

        print("Sending alias to server", end="\n\n")
        starTime = datetime.now()
        self.__socketObj.sendMessage(data)
        stopTime = datetime.now()
        
        if GET_ALIAS_WAITING_TIME - (stopTime -starTime).total_seconds() > 0:
            time.sleep(GET_ALIAS_WAITING_TIME - (stopTime -starTime).total_seconds())

        logging.debug("#######################")
        logging.debug("")

        #Get number of question
        logging.debug("Getting number of question")
        data = self.__socketObj.receiveMessage()

        numOfQuestion = 0

        if data and "num_of_questions" in data:
            numOfQuestion = data["num_of_questions"]
            print("Number of question : "+str(numOfQuestion))
            
        currNumOfQuestion = numOfQuestion
        while(currNumOfQuestion):
            currNumOfQuestion  =  currNumOfQuestion - 1

            #Listen for question
            data = self.__socketObj.receiveMessage()
            answer = ""
            sleepTime = 0
            if data and "question" in data and "choices" in data:
                #Show  current question
                #Prompt for answer
                starTime = datetime.now()
                print("Question {}: \n".format(str(numOfQuestion- currNumOfQuestion)) + str(data["question"]))
                print("Choices: " + str(data["choices"]))
                print("You have  " + str(ANSWER_WAITING_TIME) + " seconds to answer")
                answer = self.readInput("Your answer", "", ANSWER_WAITING_TIME)
                stopTime = datetime.now()
                sleepTime = ANSWER_WAITING_TIME - (stopTime -starTime).total_seconds()
                print("Answer : "+ answer, end="\n\n")
            
            if sleepTime > 0:
                time.sleep(sleepTime)

            data["answer"] = answer
            logging.debug("Data:" + str(data))

            dataWithTuple = {"addr" : self.__socketObj.getIPPort(), "message" : data}

            self.__socketObj.sendMessage(dataWithTuple)
        

        
        #get final score
        finalScore = self.__socketObj.receiveMessage()

        if finalScore:
            if "winner" in finalScore:
                print("Winner: " + str(finalScore["winner"]))

            if "highscore" in finalScore:
                print("Highscore: " + str(finalScore["highscore"]))

        self.__socketObj.closeConnection()
             
            
    
    #Ref:https://stackoverflow.com/questions/3471461/raw-input-and-timeout/3911560#3911560
    def readInput( self, caption, default, timeout = 5):
        start_time = time.time()
        sys.stdout.write('%s(%s):'%(caption, default));
        userInput = ''
        input=""
        while True:
            if msvcrt.kbhit():
                chrac = msvcrt.getche()
                if ord(chrac) == 13: # enter_key
                    break
                elif ord(chrac) >= 32: #space_char
                    userInput +=  chrac.decode("utf-8")
                    # input+=userInput
            if len(userInput) == 0 and (time.time() - start_time) > timeout:
                break

        print ("")  # needed to move to next line
        if len(userInput) > 0:
            return userInput
        else:
            return default


        


        
if __name__ == '__main__':
    #uncomment lines below for debugging

    # # setting logging util
    # format = "%(asctime)s: %(message)s"
    # logging.basicConfig(format=format, level=logging.INFO,
    #                     datefmt="%H:%M:%S")
    # logging.getLogger().setLevel(logging.DEBUG)

    address = "127.0.0.1"
    #port = findUnusedPort(address)
    port = 49152

    ClientAppCLI().run(address, port)
