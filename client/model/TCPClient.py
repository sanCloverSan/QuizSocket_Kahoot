# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 12:51:22 2020

__author__      = "Christian Patrick"
__copyright__   = "Copyright 2020, Planet Earth"
"""


import time
import json
import logging
from datetime import datetime
import socket
import os
import sys
import math

## ~/jarkom_socket_tictactoe/model/server/
currentDir = os.path.dirname(os.path.realpath(__file__))
## ~/jarkom_socket_tictactoe/model/
parentDir = os.path.dirname(currentDir)
## ~/jarkom_socket_tictactoe/
grandParentDir = os.path.dirname(parentDir)
sys.path.append(grandParentDir)

from utils.FindPort import findUnusedPort


class TCPClient():
    def __init__(self):
        self.__socketObj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.setBlocking(False)
        self.__portNumber = 0
        self.__ipAddress = ""
        self.__sockConnection = None
        self.__lastAlive = None
        self.__isConnected = False
        self.__isClosed = True

    def getSocketObj(self):
        return self.__socketObj

    def setTimeOut(self, timeout):
        self.__socketObj.settimeout(timeout)
    
    
    def setBlocking(self, blocking:bool):
        self.__socketObj.setblocking(blocking)

    def getIPPort(self):
        return (self.__ipAddress, self.__portNumber)

    def connectToServer(self, address: str = "127.0.0.1", portNumber: int = 49152):
        if(self.__socketObj):
            try:
                logging.debug("Connecting to: " + address + " " + str(portNumber))
                self.__sockConnection = self.__socketObj.connect((address, portNumber))
            except Exception as e:
                logging.debug("Failed to connect to server")
                logging.debug("Exception:" + str(e))
                self.__isConnected = False
                self.__isClosed = True
                return 0
            else:
                self.__isConnected = True
                self.__isClosed = False
                self.__lastAlive = datetime.now()
                self.__ipAddress = self.__socketObj.getsockname()[0]
                self.__portNumber = self.__socketObj.getsockname()[1]
                
                logging.debug("Client at : {addr}:{port}".format(
                    addr=self.__ipAddress, port=self.__portNumber ))
                print("Connected to : {addr}:{port}".format(
                    addr=address, port=portNumber))

                server_reply = self.__socketObj.recv(1024)
                message = server_reply.decode('utf-8')
                message = message.replace("'", "\"")
                message = json.loads(message)
                logging.debug(message)

                return message

    def isConnected(self):
        return self.__isConnected

    def isClosed(self):
        return self.__isClosed

    def sendMessage(self, message):
    #     self.__socketObj.setblocking(0)
        if self.__socketObj:
            if self.__isConnected and not self.__isClosed:
                logging.debug("Sending : \n" + str(message))
                sendData = json.dumps(message)
                sendData = sendData.encode(
                    "utf-8") if type(sendData) == str else str(sendData).encode("utf-8")

                try:
                    self.__socketObj.send(sendData)
                    lastAlive = datetime.now()
                    self.__lastAlive = lastAlive
                    return 1
                except Exception as e:
                    logging.debug("Exception:" + str(e))
                    return 0
                # finally:
                #     self.__socketObj.setblocking(1)
        return 0

    def receiveMessage(self):
        if self.__socketObj:
            if self.__isConnected and not self.__isClosed:
                try:
                    serverReply = self.__socketObj.recv(1024)
                    serverReply = serverReply.decode('utf-8')

                    logging.debug(serverReply)

                    serverReply = serverReply.replace("'", "\"")
                    replyJSON = json.loads(serverReply)

                    # logging.debug(replyJSON)
                    # logging.debug(type(replyJSON))

                    return replyJSON
                except Exception as e:
                    logging.debug(str(e))
                    return {"error", str(e)}
            elif not self.__isConnected:
                return {"error": "Not connected to server"}
            elif self.__isClosed:
                return {"error": "Connection to server closed"}
        return {"error": "Socket connection is "+str(self.__socketObj)}

    def closeConnection(self):
        if(self.__socketObj):
            if self.__isConnected and not self.__isClosed:
                logging.debug("Closing connection...")
                self.__socketObj.close()
                self.__isConnected = False
                self.__isClosed = True

if __name__ == "__main__":
    #Can be used to test with TCPServer.py
    # setting logging util
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    logging.getLogger().setLevel(logging.DEBUG)

    clientObj = TCPClient()

    # Connect to server
    address = "127.0.0.1"
    successMessage = clientObj.connectToServer(address, 49152)
    
    if successMessage and len(successMessage):
        if "status" in successMessage:
            print("Connection status:" + successMessage["status"]) 

        if "rem_acc_time" in successMessage:
            try:
                sleepTime = math.ceil(successMessage["rem_acc_time"])
                time.sleep(sleepTime)
            except Exception as e:
                logging.debug("Exception: " + str(e))
    
    data = {"test" : 456}
    clientObj.sendMessage(data)

    received = clientObj.receiveMessage()

    logging.debug("Received:" + str(received))

    clientObj.closeConnection()
