# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 12:51:22 2020

__author__      = "Christian Patrick"
__copyright__   = "Copyright 2020, Planet Earth"
"""



import os
import sys
import logging
from datetime import datetime, timedelta
import time, msvcrt

#This part of code should be placed before any module import within this project
#example: findUnusedPort module from utils.FindPort
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
grandparentdir = os.path.dirname(parentdir)
sys.path.append(grandparentdir)

from server.model.TCPServer import TCPServer
from  server.model.BankSoal import BankSoal
from utils.FindPort import findUnusedPort
import random
import select
import concurrent.futures

MAX_NUM_OF_PLAYER = 5
JOIN_GAME_WAITING_TIME = 10
GET_ALIAS_WAITING_TIME = 5
ANSWER_WAITING_TIME = 10
SUBMIT_WAITING_TIME = 5
SEND_QUESTION_NUM_TIME = 5
NUMBER_OF_QUESTION = 5

class ServerAppCLI():
    def __init__(self, addr:str, port:int):
        self.__socketObj = TCPServer(addr, port)
        self.__peername = (addr, port)
        self.__bankSoal = BankSoal()
        self.__questions = self.__bankSoal.getRandomQuestion(NUMBER_OF_QUESTION)
        self.__scores = {}
        self.__uname = ""
        

    def run(self):
        #Prompt for username
        print("Starting server app")
        logging.debug("#######################")
        print("")
        
        print("Input your username:")
        starTime = datetime.now()
        self.__uname = self.readInput("Your alias / username", "John", ANSWER_WAITING_TIME)
        stopTime = datetime.now()
        print("Username: " + self.__uname)

        urPeerName = (self.__socketObj.getIPPort())
        self.__scores[urPeerName] =  {"alias" : self.__uname, "score" : 0}
        print("")

        if ANSWER_WAITING_TIME - ( stopTime -starTime).total_seconds() > 0:
            time.sleep(ANSWER_WAITING_TIME - (stopTime -starTime).total_seconds())

        logging.debug("#######################")
        logging.debug("")

        #Waiting for other player
        self.waitingForPlayers()

        #Prompt for other users alias
        self.getClientsAlias()

        #Send num of question
        logging.debug("Sending number of question")
        data = {"num_of_questions": NUMBER_OF_QUESTION}
        self.__socketObj.sendMessage(data)

        time.sleep(SEND_QUESTION_NUM_TIME)

        count = 0
        print("Starting the Game")

        while (len(self.__questions)):
            print("Question {}: \n".format(str(count+1)))  
            count+=1
              
            currentQuestion = self.getRandomQuestion()
            #Send current question to all participant
            self.sendQuestion(currentQuestion)

            #Show self current question
            #Prompt for answer
            print("Question: " + str(currentQuestion["question"]))
            print("Choices: " + str(currentQuestion["choices"]))
            print("You have  " + str(ANSWER_WAITING_TIME) + " seconds to answer")
            answer = self.readInput("Your answer", "", ANSWER_WAITING_TIME)
            print("Answer : "+ answer)
            print("")

            if answer == currentQuestion["correct_answer"]:
                self.__scores[self.__peername]["score"] = self.__scores[self.__peername]["score"] + 1
            
            #Get answers from other participant
            self.getAnswers(currentQuestion)
        
        #Calculate final score and send to clients
        self.sendFinalScore()

        self.__socketObj.closeConnection()


        #Close connection

    def waitingForPlayers(self):
        print("Waiting for player to join")
        startTime = datetime.now()
        deltaTime = datetime.now()
        self.__socketObj.startListen(MAX_NUM_OF_PLAYER)
        while (deltaTime - startTime).total_seconds() < JOIN_GAME_WAITING_TIME \
            and self.__socketObj.getNumberOfClients() <= MAX_NUM_OF_PLAYER:
            print("Time left:" + str(JOIN_GAME_WAITING_TIME - (deltaTime - startTime).total_seconds()))
            self.__socketObj.setTimeOut(JOIN_GAME_WAITING_TIME - (deltaTime - startTime).total_seconds())
            self.__socketObj.acceptConnection(JOIN_GAME_WAITING_TIME)
            # self.__socketObj.setTimeOut(None)
            print("Num. of Player:" + str(self.__socketObj.getNumberOfClients() + 1))
            deltaTime = datetime.now()
        self.__socketObj.setTimeOut(None)
        logging.debug("Fill initial score for each player")
        clients = self.__socketObj.getClientConnection()
        for client in clients:
            peername = client.getpeername()
            self.__scores[peername] = {"alias" : "", "score" : 0}
            logging.debug(str(peername)+": "+str(0))

    
    def readInput( self, caption, default, timeout = 5):
        #Ref:https://stackoverflow.com/questions/3471461/raw-input-and-timeout/3911560#3911560
        start_time = time.time()
        sys.stdout.write('%s(%s):'%(caption, default));
        userInput = ''
        while True:
            if msvcrt.kbhit():
                chrac = msvcrt.getche()
                if ord(chrac) == 13: # enter_key
                    break
                elif ord(chrac) >= 32: #space_char
                    userInput +=  chrac.decode("utf-8")
            if len(userInput) == 0 and (time.time() - start_time) > timeout:
                break

        print ("")  # needed to move to next line
        if len(userInput) > 0:
            return userInput
        else:
            return default

    def sendQuestion(self, question):
        clients = self.__socketObj.getClientConnection()
        checkQuestion = dict(question)
        checkQuestion.pop("correct_answer")
        logging.debug("Sending question:" + str(checkQuestion))
        self.__socketObj.sendMessage(checkQuestion)

    def getClientsAlias(self):
        print("Get Aliases from clients")
        
        startTime = datetime.now()
        deltaTime = datetime.now()

        aliases = []

        while (deltaTime - startTime).total_seconds() < GET_ALIAS_WAITING_TIME:
            for client in self.__socketObj.getClientConnection(): 
                
                timeout = GET_ALIAS_WAITING_TIME // len(self.__socketObj.getClientConnection())
                response = self.__socketObj.receiveMessage(client, timeout)

                if response and not "error" in response:
                    logging.debug(response)
                    aliases.append(response)
            
            deltaTime = datetime.now()
        

        for clientAlias in aliases:
            if clientAlias and "addr" in clientAlias:
                logging.debug(str(clientAlias))
                peerName = (clientAlias["addr"][0], clientAlias["addr"][1])
                alias = clientAlias["alias"]

                if peerName in self.__scores:
                    self.__scores[peerName]["alias"] = alias
        
        logging.debug("Score : "+str(self.__scores))


    def getAnswers(self, question):
        logging.debug("Get Answers:")
        
        startTime = datetime.now()
        deltaTime = datetime.now()

        serverSocket = self.__socketObj.getSocket()

        answers = []
        while (deltaTime - startTime).total_seconds() < SUBMIT_WAITING_TIME:
            for client in self.__socketObj.getClientConnection(): 
                timeout = SUBMIT_WAITING_TIME // len(self.__socketObj.getClientConnection())
                response = self.__socketObj.receiveMessage(client, timeout)

                if response and not "error" in response:
                    logging.debug(response)
                    answers.append(response)
            
            deltaTime = datetime.now()
        
        
        for answer in answers:
            if answer and "addr" in answer:
                logging.debug("RCVD ANSWER: " + str(answer))
                peerName = (answer["addr"][0], answer["addr"][1])
                message = answer["message"]

                if peerName in self.__scores:
                    if "question" in message and "answer" in message:
                        rcvdQuestion = message["question"] 
                        answer = message["answer"]

                        if answer == question["correct_answer"]:
                            self.__scores[peerName]["score"] = self.__scores[peerName]["score"] + 1
                                



    def sendScores(self):
        
        clients = self.__socketObj.getClientConnection()
        for client in clients:
            self.__socketObj.sendMessage(self.__scores, client)

    def sendFinalScore(self):
        #Send Highscore for client
        data = {"winner": None , "highscore":0}

        for peername in self.__scores:
            currentPlayerData = self.__scores[peername]
            if data["highscore"] < currentPlayerData["score"]:
                data["highscore"] = currentPlayerData["score"]
                data["winner"] = "Player" if currentPlayerData["alias"] == "" else currentPlayerData["alias"] \
                    + " at "+str(peername[0]) + ":" +str(peername[1]) 

        
        
        clients = self.__socketObj.getClientConnection()
        for client in clients:
            self.__socketObj.sendMessage(data)

        print("Winner : " + str(data["winner"]))                    
        print("Score : "+str(data["highscore"]))


    def getRandomQuestion(self):
        # print(self.__questions)
        randIdx = random.randrange(0, len(self.__questions))
        # print(randIdx)
        randomQuestion = self.__questions[randIdx]
        self.__questions.pop(randIdx)
        return randomQuestion

            
if __name__ == "__main__":
    # uncomment for debugging
    
    # # setting loggin util
    # format = "%(asctime)s: %(message)s"
    # logging.basicConfig(format=format, level=logging.INFO,
    #                     datefmt="%H:%M:%S")
    # logging.getLogger().setLevel(logging.DEBUG)

    address = "127.0.0.1"
    #port = findUnusedPort(address)
    port = 49152

    ServerAppCLI(address, port).run()



