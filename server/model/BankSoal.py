# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 12:51:22 2020

__author__      = "Christian Patrick"
__copyright__   = "Copyright 2020, Planet Earth"
"""



import os
import sys
import json
import logging

#This part of code should be placed before any module import within this project
#example: findUnusedPort module from utils.FindPort
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
grandparentdir = os.path.dirname(parentdir)
sys.path.append(grandparentdir)

import random
import pprint

class BankSoal():
    def __init__(self):
        dataSoal = ""
        with open(currentdir+'/BankSoal.json', 'r') as myfile:
            dataSoal =  myfile.read()

        dataSoal = dataSoal.replace("'", "\"")
        dataSoal = json.loads(dataSoal)
        self.__questions = dataSoal["soal"]


    def getRandomQuestion(self, number:int):
        # logging.debug(self.__questions)
        randomQuestion = random.sample(self.__questions, number)
        
        return randomQuestion

if __name__ == "__main__":
    #Can be used to test with TCPClient.py
    # setting loggin util
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    logging.getLogger("rich.logging.RichHandler")
    logging.getLogger().setLevel(logging.DEBUG)
    bankSoal = BankSoal()

    logging.debug(bankSoal.getRandomQuestion(3))
