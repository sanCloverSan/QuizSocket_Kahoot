# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 12:51:22 2020

__author__      = "Christian Patrick"
__copyright__   = "Copyright 2020, Planet Earth"
"""



import os
import sys
import json
import socket
import logging
import select
import time
from datetime import datetime, timedelta

#This part of code should be placed before any module import within this project
#example: findUnusedPort module from utils.FindPort
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
grandparentdir = os.path.dirname(parentdir)
sys.path.append(grandparentdir)

from utils.FindPort import findUnusedPort


class TCPServer ():
    def __init__(self, address:str="127.0.0.1", portNumber: int = 49152):
        self.__socketObj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.setBlocking(False)
        self.__portNumber = portNumber
        self.__ipAddress = address
        try:
            self.__socketObj.bind((address, portNumber))
        except:
            logging.debug("Failed to connect to".format(address, portNumber))
        else:
            logging.debug("Server will running at {}:{}".format(address, portNumber))

        self.__isClosed = False
        self.__isListening = False
        self.__lastConnectionTime = datetime.now()
        self.__clientConnections = []
        self.__clientAddress = ""
        self.__clientPort = 0

    def startListen(self , numOfClient):
        if(self.__socketObj):
            self.__socketObj.listen(numOfClient)
            self.__isListening = True
            self.__isClosed = False
        
    def setTimeOut(self, time):
        self.__socketObj.settimeout(time)
    
    def getSocket(self):
        return self.__socketObj

    def setBlocking(self, blocking:bool):
        self.__socketObj.setblocking(blocking)

    def getClientConnection(self):
        return self.__clientConnections

    def acceptConnection(self, timeout):
        if self.__socketObj:
            deltaLastConnectionTime = datetime.now() - self.__lastConnectionTime
            durationInSeconds = deltaLastConnectionTime.total_seconds()
            logging.debug("Last connection: {}".format(
                str(deltaLastConnectionTime)))

            if self.__isListening and not self.__isClosed:
                try:
                    startTime = datetime.now()

                    tempConnection, addr = self.__socketObj.accept()

                    deltaLastConnectionTime = startTime - self.__lastConnectionTime
                    durationInSeconds = deltaLastConnectionTime.total_seconds()
                    logging.debug("Since last connection: "+str(durationInSeconds))

                    # logging.debug(tempConnection)
                    if tempConnection and addr:
                        print("Connection from: "+str(addr))
                        self.__clientConnections.append( tempConnection)
                        sendingTime = datetime.now()

                        remainingAcceptTime = timeout -  (sendingTime - startTime).total_seconds()

                        sendData = {"status" : "Success", "rem_acc_time":remainingAcceptTime}
                        successMessage = json.dumps(sendData)
                        successMessage = successMessage.encode("utf-8")
                        tempConnection.send(successMessage)
                        lastAlive = datetime.now()

                        self.__lastConnectionTime = datetime.now()
                except socket.timeout:
                        logging.debug("Exception: Socket Time Out")

                
                # time.sleep(1)
            else:
                self.__isListening = False
                self.__isClosed = True

    def sendMessage(self, message):
        if self.__socketObj:
            if not self.__isClosed and len(self.__clientConnections):
                for clientConnection in self.__clientConnections:
                    logging.debug("Sending : \n" + str(message))
                    sendData = json.dumps(message)
                    sendData = sendData.encode(
                        "utf-8") if type(sendData) == str else str(sendData).encode("utf-8")
                    try:
                        clientConnection.send(sendData)
                        return 1
                    except socket.error:
                        logging.debug(socket.error)
                        return 0

    def receiveMessage(self, clientConn, timeout):
        if self.__socketObj:
            if not self.__isClosed and len(self.__clientConnections):
                if clientConn:
                    try:
                        # self.__socketObj.settimeout(timeout)
                        clientConn.settimeout(timeout)
                        receivedMessage = clientConn.recv(1024)
                        receivedMessage = receivedMessage.decode('utf-8')
                        
                        logging.debug(receivedMessage)

                        receivedMessage = receivedMessage.replace("'", "\"")

                        messageJSON = json.loads(receivedMessage)
                        clientConn.settimeout(None)
                        # self.__socketObj.settimeout(None)
                        return messageJSON
                    except Exception as e:
                        logging.debug("Exception:" + str(e))
                        return {"error": str(socket.error)}

    def getClient(self, idx:int):
        if idx >=0 and idx < len(self.__clientConnections):
            return self.__clientConnections[idx]
    
    def getIPPort(self):
        return (self.__ipAddress, self.__portNumber)

    def getNumberOfClients(self):
        return len(self.__clientConnections)

    def closeConnection(self):
        if(self.__socketObj):
            logging.debug("Closing connection...")
            self.__socketObj.close()
            self.__isListening = False
            self.__isClosed = True

if __name__ == "__main__":
    #Can be used to test with TCPClient.py
    # setting loggin util
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    logging.getLogger().setLevel(logging.DEBUG)

    # Initiate a server
    address = "127.0.0.1"
    portNumber = findUnusedPort(address)
    serverObj = TCPServer(address, portNumber)
    # start listen for connection from client
    serverObj.startListen()

    data = {"test" : 123}

    serverObj.acceptConnection(0)

    receivedMessage = serverObj.receiveMessage(serverObj.getClient(0), None)
    logging.debug("Received:" + str(receivedMessage))

    serverObj.sendMessage(data)
    serverObj.closeConnection()
