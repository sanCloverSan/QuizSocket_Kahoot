#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 21:10:44 2020

{Description}
{License_info}

__author__      = "Christian Patrick"
__copyright__   = "Copyright 2021, Planet Earth"
"""

# Built-in/Generic Imports
import socket
import sys
import psutil
import logging

START_NUMBER = 49152
END_NUMBER = 65535


def findUnusedPort(ipAddress: str = "127.0.0.1"):
    portNumber = -1
    if not len(ipAddress):
        ipAddress = "127.0.0.1"

    if not isValidIpv4Address(ipAddress):
        logging.debug("Address Not Valid.")
        return portNumber

    logging.debug("Finding port number for address: {}".format(ipAddress))
    try:
        for port in range(START_NUMBER, END_NUMBER):
            logging.debug("Checking port: " + str(port))
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((ipAddress, port))
            # logging.debug("Result: ",result)
            if result == 10061:
                logging.debug("Port {}: 	 Unused".format(port))
                sock.close()
                portNumber = port
                break
            sock.close()
    except KeyboardInterrupt:
        logging.debug("You pressed Ctrl+C")
        sys.exit()

    except socket.gaierror:
        logging.debug('Hostname could not be resolved. Exiting')
        sys.exit()

    except socket.error:
        logging.debug("Couldn't connect to server")
        sys.exit()
    finally:
        return portNumber


def isPortUsed(ipAddress: str = "127.0.0.1", portNumber: int = START_NUMBER):
    status = 1
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((ipAddress, portNumber))
    # logging.debug("Result: ",result)
    if result == 10061:
        logging.debug("Port {}: 	 Unused".format(portNumber))
        status = 0
    sock.close()
    return status


def isValidIpv4Address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False

    return True

# Ref: https://kbarik.wordpress.com/2020/01/16/get-ip-address-mac-address-and-network-interface-name-using-python-os-independent/
def getActiveIPAdressesAndNIC():
    hostname = socket.gethostname()
    ipAddresses = socket.gethostbyname_ex(hostname)[-1]
    nics = psutil.net_if_addrs()
    ipNICTuples = []

    for nic in nics:
        for snicaddr in nics[nic]:
            if snicaddr.family == socket.AF_INET:
                address = snicaddr.address
                interface = nic
                ipNICTuples.append((interface, address))
    return ipNICTuples



if __name__ == "__main__":
    # setting logging util
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    logging.getLogger().setLevel(logging.DEBUG)

    logging.debug(findUnusedPort(), end='\n\n')
    ipNICTuples = getActiveIPAdressesAndNIC()
    for interface, ip in ipNICTuples:
        logging.debug(interface+" : " + "is valid ip ("+ip +
              ")" if isValidIpv4Address(ip) else "not valid ip ("+ip+")")